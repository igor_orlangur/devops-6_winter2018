#!/usr/bin/env bash
#
#Script for tranfering local files
#

set -e

#Set variables
LOG="$HOME/$(basename ${0%.sh}).log"
PID=$$
CONTROL="/var/tmp/$(basename ${0})"

##Create help function
function usage {
    echo "Script $0 is for tranfering files from destionation directory"
    echo -e "Can: transfer files, create reports, create archives"
    echo -e "Usage:\n\t$0 transfer from_directory to_directory"
    echo -e "\t$0 report time_span(in days ago)"
    echo -e "\t$0 archive destination_directory time_span(in days ago)"
    echo "Logfile - $LOG"
}

#Function for transfer files from one directory to other
function transfer {
  touch $CONTROL
  if [ "$(grep -c "$FROM" $CONTROL)" -ne 0 ]; then
    echo "We already transfer files from $1"
    exit 1
  fi
  if [ ! -d "$FROM" ]; then
    if [ ! -f "$FROM" ]; then
      echo "\033[0;31mERROR\e[0m Tranfer directory or file should exist!"
      usage
      exit 2
    fi
  fi
  if [ ! -d "$TO_DIR" ]; then
    mkdir -p "$TO_DIR"
    if [ "$(echo $?)" -ne "0" ]; then
      exit 3
    fi
  fi

  echo -e "$FROM\n" >> $CONTROL
  echo -e "$(date -Isecond) $PID Start transfer from $FROM to $TO_DIR\n" >> $LOG



  #Transfer part
  for FILE in $(find "$FROM" -type f); do
    # Check uuid of file
    if [[ $(tail -1 $FILE|grep -E "[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{8}" -c) -eq 0 ]]; then
      echo -ne "$(date -Isecond) $PID \033[0;31mEROOR! $FILE is not full.\n" >> $LOG
      continue
    fi
    FILENAME=$(head -1 $FILE|awk '{print $1}')
    FILEDATE=$(head -1 $FILE|awk '{$1="";print $0}'|sed -e "s/ /_/g" -e "s/:/./g")
    TRANSFER_TIME=$(date +%s)
    DEST="$TO_DIR/${FILENAME}${FILEDATE}_${TRANSFER_TIME}"
    mv $FILE $DEST
    echo -ne "$(date -Isecond) $PID \033[0;32mSUCCESS!\e[0m $FILE moved to $DEST\n" >> $LOG
  done

  echo -e "$(date -Isecond) $PID End transfer\n" >> $LOG
  sed -i "s;${FROM};;" $CONTROL
}


#Function for creation report about transfered files
function report {
  echo -ne "$(date -Isecond) $PID Create report for $TIME_SPAN days ago" >> $LOG
  if [[ -f "$LOG" ]]; then
    grep "$(date +%Y-%m-%d -d "$TIME_SPAN day ago")" $LOG| \
     awk '{print $4}'|grep -oP '.*(?=/)'|sort|uniq -c|sort -rh
  fi
}

#Function for making archive for one of date
function archive {
  DEFAULT_TIME=$(date +%s -d "$TIME_SPAN day ago")
  if [[ -d $DEST_DIR ]]; then
    for FILE in $(find $DEST_DIR -type f ! -name '*.tar' -exec basename -a {} \;|rev|cut -d "_" -f 4-|rev|sort|uniq); do
      # Create variables for listing files
      DATES=$(find $DEST_DIR -type f ! -name '*.tar' | grep $FILE | awk -F"_" '{print $NF}')
      LIST_OF_FILES=/var/tmp/${FILE}.list
      for DATE in $DATES; do
         if [[ $DATE < $DEFAULT_TIME ]]; then
	   echo -e "$(find $DEST_DIR -type f ! -name '*.tar' -exec basename -a {} \;| grep "$FILE.*$DATE")" >> $LIST_OF_FILES
         fi
       done
       if [[ -f $LIST_OF_FILES ]]; then
         START_TIME=$(awk -F"_" '{print $NF}' $LIST_OF_FILES | sort | head -1)
         END_TIME=$(awk -F"_" '{print $NF}' $LIST_OF_FILES | sort | tail -1)
         ARCHNAME=${FILE}_${START_TIME}-${END_TIME}.tar
         cd $DEST_DIR
         tar -cf $ARCHNAME --files-from ${LIST_OF_FILES}
         rm -f ${LIST_OF_FILES}
         echo "Create archive ${DEST_DIR}/${ARCHNAME}" >> $LOG
       else
         echo "List does not exist. Noting to do." >> $LOG
       fi
     done
  fi
}

#Check variables
if [ "$1" == "-h" -o "$1" == "--help" ]; then
  usage
  exit 0
elif [ "$1" = "transfer" ]; then
  FROM=$2
  TO_DIR=$3
  transfer
elif [ "$1" = "report" ]; then
  TIME_SPAN=$2
  report
elif [ "$1" = "archive" ]; then
  DEST_DIR=$2
  TIME_SPAN=$3
  archive
else
  usage
  exit 1
fi
