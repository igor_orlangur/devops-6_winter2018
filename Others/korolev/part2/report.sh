#!/bin/bash
source ./moover.conf --source-only

echo " Enter start_timestamp for the report in a format YYYYMMDD_HHMMSS (Ex. 20190127_133516)"
read start
echo " Enter final_timestamp for the report in a format YYYYMMDD_HHMMSS (Ex. 20190127_133523)"
read stop

awk -v from="$start" -v to="$stop" '$1>=from && $1<=to' moved.log > precooked_report
awk -F'_' '{print $3}' precooked_report | sort | uniq -c | sort > moved.rpt
rm precooked_report