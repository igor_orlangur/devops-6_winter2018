#!/bin/bash
. config
for object in $DIR/*
do
	UUID="^.{8}(-.{4}){3}-.{12}$"
	if [[ $(tail -n 1 $object) =~ $UUID ]]; then
	name=$(head -n 1 $object | cut -f1)
	date=$(head -n 1 $object | cut -f2)
	convert_date=$(date -d "@$( stat -c '%Y' $object )" +'%Y%m%d_%H%M%S')
	rename="${name}_${date}_${convert_date}"
	mv $object "./${RESULT_DIR}/$rename"
	echo -e "$(date) - object \"$(echo $object | cut -c $(expr length $DIR + 2)-)\" moved to \"$(echo $RESULT_DIR | cut -c 3-)\" and named \"$rename\"\n" >> $LOG
	fi
done
