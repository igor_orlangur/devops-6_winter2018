#!/bin/bash
function config_template {
TEMPL=$1
RESULT=$2
if	[[ -z "$TEMPL" ]]; then
	echo "There are no arguments, exit script"
	exit
fi
if	[[ ! -s "$TEMPL" ]]; then
	echo "file is empty or not exist, exit script"
	exit
fi
if	[[ ! -f "$TEMPL" ]]; then
	echo "$TEMPL is not a template file"
	exit
fi
if	[[ -z "$RESULT" ]]; then
	echo "The name of the result file is not set"
	read -p 'Please set the name: ' RESULT
fi
if	[[ -f "$RESULT" ]]; then
	echo "file '$RESULT' already exist. Do you want to overwrite it?"
	while true; do
		read -p "(Y/N) " -n 1 answer
		echo 
	case "$answer" in
		[Nn]) exit;;
		[Yy]) cp /dev/null "$RESULT"; break;;
		*) echo " Only Y or N arguments are available"
	esac
done
else touch $RESULT
	echo "'$RESULT' was created"
fi
cat $TEMPL | while read STRING; do
eval echo $STRING >> $RESULT
done
}
if [[ "$(basename $0)" == "config_script.sh" ]]; then
	config_template "$@"
fi
