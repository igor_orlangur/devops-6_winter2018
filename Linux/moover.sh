#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/moover.conf --source-only
OUTPUT=$SOURCEDIR
INPUT=$TARGETDIR
LOG=$LOGFILENAME

if [ ! -d "$OUTPUT" ]; then
	eval mkdir $OUTPUT
fi

for file in $INPUT/*
do
	if [ ! -e $file.lck ]
	then
		touch $file.lck
		header=$(head -n 1 $file)
		footer=$(tail -n 1 $file)
	
		pattern="^\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"
		if [[ $footer =~ $pattern ]]
		then
			obj_name=$(head -n 1 $file | awk '{print $1}')
			obj_date=$(head -n 1 $file | awk '{print $2}')
			formated_date=$(echo $obj_date | awk -F- '{printf "%s%s%s\n",$3,$2,$1}')
			obj_date_mod=$(date -d "@$( stat -c '%Y' $file )" +'%Y%m%d_%H%M%S')
			newfilename="${formated_date}_${obj_name}_${obj_date_mod}"
			eval mv $file $OUTPUT/$newfilename && echo "$(date +"%Y%m%d_%H%M%S") successfully moved $newfilename" >> $LOG && rm $file.lck
		else
			#echo "$(date +"%Y%m%d_%H%M%S") is still downloading $file" >> $LOG
			rm $file.lck
			continue
		fi
	else
		continue
	fi
done
# rm $INPUT/*.lck
